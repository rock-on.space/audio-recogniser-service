import unittest
import wave
from dataclasses import dataclass
from io import BytesIO

from pydub import AudioSegment

from src.wav2ogg import convert_wav2ogg


@dataclass
class WavData:
    audio_data: bytes
    sample_rate: float
    channels: int = 1
    sample_width: int = 2


class TestWavConversion(unittest.TestCase):
    def load_sample(self, sample_name: str, n_seconds: int = 15) -> WavData:
        with open(f"tests/test_files/{sample_name}_{n_seconds}.wav", "rb") as raw_fp:
            wav = wave.open(raw_fp)
            samples_requested = n_seconds * wav.getframerate()
            return WavData(
                audio_data=wav.readframes(samples_requested),
                sample_rate=wav.getframerate(),
                channels=wav.getnchannels(),
                sample_width=wav.getsampwidth(),
            )

    def load_expected_result(self, sample_name: str, n_seconds: int) -> BytesIO:
        with open(
            f"tests/test_files/{sample_name}_result_{n_seconds}.ogg", "rb"
        ) as raw_fp:
            return BytesIO(raw_fp.read())

    def test_good_wav_mono(self):
        # Get test data
        sample = self.load_sample("commando-mono", 15)
        expected_raw = self.load_expected_result("commando-mono", 15)
        # Call function under test
        ogg_raw = convert_wav2ogg(
            sample.audio_data, sample.sample_rate, sample.channels, sample.sample_width
        )
        # Validate Result
        expected_raw.seek(0)
        ogg_raw.seek(0)
        expected_segment = AudioSegment.from_ogg(expected_raw)
        ogg_segment = AudioSegment.from_ogg(ogg_raw)
        self.assertEqual(expected_segment.channels, ogg_segment.channels)
        self.assertEqual(expected_segment.sample_width, ogg_segment.sample_width)
        self.assertEqual(expected_segment.frame_rate, ogg_segment.frame_rate)
        self.assertEqual(expected_segment[:15000], ogg_segment[:15000])

    def test_bad_wav_mono(self):
        # Get test data
        sample = self.load_sample("cube3-InsideMe-mono", 30)
        expected_raw = self.load_expected_result("commando-mono", 15)
        # Call function under test
        ogg_raw = convert_wav2ogg(
            sample.audio_data, sample.sample_rate, sample.channels, sample.sample_width
        )
        # Validate Result
        expected_raw.seek(0)
        ogg_raw.seek(0)
        expected_segment = AudioSegment.from_ogg(expected_raw)
        ogg_segment = AudioSegment.from_ogg(ogg_raw)
        self.assertEqual(expected_segment.channels, ogg_segment.channels)
        self.assertEqual(expected_segment.sample_width, ogg_segment.sample_width)
        self.assertEqual(expected_segment.frame_rate, ogg_segment.frame_rate)
        self.assertNotEqual(expected_segment[:15000], ogg_segment[:15000])

    def test_good_wav_stereo(self):
        # Get test data
        sample = self.load_sample("commando-stereo", 15)
        expected_raw = self.load_expected_result("commando-stereo", 15)
        # Call function under test
        ogg_raw = convert_wav2ogg(
            sample.audio_data, sample.sample_rate, sample.channels, sample.sample_width
        )
        # Validate Result
        expected_raw.seek(0)
        ogg_raw.seek(0)
        expected_segment = AudioSegment.from_ogg(expected_raw)
        ogg_segment = AudioSegment.from_ogg(ogg_raw)
        self.assertEqual(expected_segment.channels, ogg_segment.channels)
        self.assertEqual(expected_segment.sample_width, ogg_segment.sample_width)
        self.assertEqual(expected_segment.frame_rate, ogg_segment.frame_rate)
        self.assertEqual(expected_segment[:15000], ogg_segment[:15000])

    def test_good_wav_mono_96khz(self):
        # Get test data
        sample = self.load_sample("cube3-InsideMe-mono-96", 13)
        expected_raw = self.load_expected_result("cube3-InsideMe-mono-96", 13)
        # Call function under test
        ogg_raw = convert_wav2ogg(
            sample.audio_data, sample.sample_rate, sample.channels, sample.sample_width
        )
        # Validate Result
        expected_raw.seek(0)
        ogg_raw.seek(0)
        expected_segment = AudioSegment.from_ogg(expected_raw)
        ogg_segment = AudioSegment.from_ogg(ogg_raw)
        self.assertEqual(expected_segment.channels, ogg_segment.channels)
        self.assertEqual(expected_segment.sample_width, ogg_segment.sample_width)
        self.assertEqual(expected_segment.frame_rate, ogg_segment.frame_rate)
        self.assertEqual(expected_segment[:10000], ogg_segment[:10000])


if __name__ == "__main__":
    unittest.main()
