This is the API for the users service. It will be responsible for storing and providing things like friends, biographies, block lists, privacy settings, and preferences.

It will not use authentication (for now) - This is called a "squishy centre" which is not advised for production, but at this stage of development it improves developer experience.

Draft doc: https://app.swaggerhub.com/apis/Rock-On.Space/Audio-Recognizer/1.0.0
