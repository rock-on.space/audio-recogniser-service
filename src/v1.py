import re

from fastapi import APIRouter, HTTPException
from starlette.requests import Request

from multipart_mixed import receive_multipart
from recognizer_handler import recognize
from sensor_service import submit_music_meta
from wav2ogg import convert_wav2ogg


router = APIRouter(
    prefix="/v1",
    tags=["v1"],
)

REGEX_BOUNDARY = re.compile(r"multipart/mixed; boundary=\"(\S*)\"")


@router.post("/sensor/{uid}")
async def receive_audio(uid: str, request: Request):
    if not (
        content_type := request.headers.get("content-type")
    ) or not REGEX_BOUNDARY.match(content_type):
        raise HTTPException(status_code=400, detail="unrecognized content-type")

    if (raw_boundary := REGEX_BOUNDARY.search(content_type)) is None:
        raise HTTPException(status_code=400, detail="Cannot find boundary")
    boundary = b"--" + raw_boundary.group(1).encode()
    json_data, audio_data = await receive_multipart(request, boundary)
    if (json_data is None) or (audio_data is None):
        raise HTTPException(status_code=400, detail="unprocessable data")

    sample_rate = json_data.get("sample_rate")
    ogg = convert_wav2ogg(audio_data, sample_rate)
    music_meta = await recognize(ogg)
    if music_meta is not None:
        await submit_music_meta(uid, music_meta)

    return {}, 200
