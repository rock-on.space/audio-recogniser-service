import wave
from io import BytesIO

from pydub import AudioSegment


def convert_wav2ogg(
    audio_data: bytes, sample_rate: float, channels: int = 1, sample_width: int = 2
):
    wav_file = BytesIO()
    with wave.open(wav_file, mode="wb") as f:
        f.setnchannels(channels)
        f.setsampwidth(sample_width)
        f.setframerate(sample_rate)
        f.writeframes(audio_data)
    wav_file.seek(0)
    ogg = BytesIO()
    # convert to ogg
    sound = AudioSegment.from_wav(wav_file)
    sound.export(ogg, format="ogg")
    return ogg
