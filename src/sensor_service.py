import httpx

from config import settings


async def submit_music_meta(sensor_uid, music_meta):
    headers = {
        "Content-Type": "application/json",
    }
    async with httpx.AsyncClient() as client:
        result = await client.post(
            f"{settings.sensor_url}/v1/sensor/{sensor_uid}/music_detection",
            headers=headers,
            content=music_meta.json().encode("ascii"),
        )
    return result.status_code
