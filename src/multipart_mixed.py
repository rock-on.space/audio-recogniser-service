import json
import re

from starlette.requests import Request

REGEX_CONTENT_TYPE = re.compile(r"Content-Type:\s*((\s?\S+)*)\r\n\r\n".encode())


async def receive_multipart(request: Request, boundary: bytes):
    # This could be made to stream the body as it comes, though that is probably unnecessary optimization.
    body = await request.body()
    parts = split_on_boundary(body, boundary)
    json_data = {}
    audio_data = b""
    for part in parts:
        charset, preamble, content_type = parse_section_header(part)
        if charset is None:
            return None, None
        part = part[len(preamble) :]
        if charset:
            part.decode(charset[0][0].split("=")[1])
        if content_type == "application/json":
            json_data = json.loads(part)
        elif content_type == "audio/wav":
            audio_data = part
    return json_data, audio_data


def split_on_boundary(body: bytes, boundary: bytes):
    parts = (
        part
        for part in (part.strip(b"\r\n").strip(b"\n") for part in body.split(boundary))
        if part not in (b"", b"--")
    )
    return parts


def parse_section_header(part: bytes):
    if (raw_content_type := REGEX_CONTENT_TYPE.match(part)) is None:
        return None, None, None
    content_type = (
        raw_content_type.group(0)
        .decode("ascii")
        .strip("Content-Type:")
        .lstrip(" ")
        .strip("\r\n")
    )
    modifiers = content_type.split("; ")[1:]
    charset = [re.search(r"charset=(\S*)", modifier) for modifier in modifiers]
    preamble = raw_content_type.group(0)
    return charset, preamble, content_type.split("; ")[0]
