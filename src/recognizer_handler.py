import json
from io import BytesIO

import httpx

from config import settings
from models import MusicMeta


async def recognize(ogg: BytesIO):
    data = {"return": "apple_music,spotify", "api_token": settings.audd_token}
    files = {"file": ogg}

    async with httpx.AsyncClient() as client:
        api_result = await client.post(settings.audd_url, data=data, files=files)
        audd_data = json.loads(
            api_result.content.decode(api_result.default_encoding)  # type:ignore
        )

    if audd_data.get("status", "failure") != "success":
        return None

    result = audd_data.get("result", {})

    genre_names = result.get("apple_music", {}).get("genreNames") or ["unknown"]
    title = result.get("title", "") or "unknown"
    artists = []
    image = ""
    release_date = ""
    if "spotify" in result:
        for artist in result["spotify"]["artists"]:
            artists.append(artist["name"])
        image = result["spotify"]["album"]["images"][0]
        release_date = result["spotify"]["album"]["release_date"]
    if image == "" and "apple_music" in result:
        image = result["apple_music"]["artwork"]["url"]
    if len(artists) == 0:
        artists.append(result.get("artist", "") or "unknown")
    music_meta = MusicMeta(
        name=title,
        album_image=image,
        release_date=release_date,
        artists=artists,
        genres=genre_names,
    )
    return music_meta
