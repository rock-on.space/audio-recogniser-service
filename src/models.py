from __future__ import annotations

from typing import List

from pydantic import BaseModel

from config import settings


class MusicMeta(BaseModel):
    name: str
    album_image: str
    release_date: str
    artists: List[str]
    genres: List[str]


class Health(BaseModel):
    service_name: str = settings.app_name
    version_number: str = settings.version_number
    errors: List[str]
