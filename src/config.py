from pydantic import BaseSettings


class Settings(BaseSettings):
    app_name: str = "SensorAttestationService"
    version_number: str = "local"
    audd_token: str = "REPLACEME"
    audd_url: str = "https://api.audd.io/"
    sensor_url: str = "http://localhost:8001/"


settings = Settings()
