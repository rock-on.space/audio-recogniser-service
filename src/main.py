import logging

import httpx
from fastapi import FastAPI

import v1
from config import settings
from models import Health

app = FastAPI()
app.include_router(v1.router)


@app.get("/healthz", response_model=Health)
async def healthz():
    response = Health(errors=[])
    async with httpx.AsyncClient() as client:
        try:
            user_service_result = await client.get(
                settings.audd_url,
            )
            user_service_result.raise_for_status()
        except httpx.ConnectError as e:
            logging.warning("audd: %s", e.args)
            response.errors.append("audd")
        except httpx.HTTPStatusError as e:
            logging.warning("audd: %s", e.args)
            response.errors.append("audd")
        try:
            sensor_service_result = await client.get(
                f"{settings.sensor_service_url}/healthz",
            )
            sensor_service_result.raise_for_status()
        except httpx.ConnectError as e:
            logging.warning("sensor_service: %s", e.args)
            response.errors.append("sensor_service")
        except httpx.HTTPStatusError as e:
            logging.warning("sensor_service: %s", e.args)
            response.errors.append("sensor_service")
    return response


@app.get("/alive")
async def alive():
    return {"message": "Hello World"}
